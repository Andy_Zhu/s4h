from django.contrib import admin
from .models import Blog
from .models import Products, ProductType, Block1, Block2, Block3, Block4, Address

#admin.site.register(Blog)
@admin.register(Blog)
class BlogAdmin(admin.ModelAdmin):
    list_display=("id","title","author","create_time","update_time")


@admin.register(Products)
class ProductsAdmin(admin.ModelAdmin):
    list_display=("name","product_type")


admin.site.register(ProductType)
admin.site.register(Block1)
admin.site.register(Block2)
admin.site.register(Block3)
admin.site.register(Block4)
admin.site.register(Address)

