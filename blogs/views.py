from django.shortcuts import render
from .models import Products, ProductType, Block1, Block2, Block3, Block4, Address

def index(request):
    context = {}
    #context['block1'] = Block1.objects.get(pk=1)
    context['block1'] = Block1.objects.all()[0]
    context['block2'] = Block2.objects.all()[0]
    context['block3'] = Block3.objects.all()[0]
    context['block4'] = Block4.objects.all()
    context['address'] = Address.objects.all()[0]

    return render(request, "blogs/home.html",context)


def products(request):
    context = {}
    context['products']= Products.objects.all()
    context['types'] = ProductType.objects.all()
    context['address'] = Address.objects.all()[0]
    return render(request, "blogs/products.html", context)
