from django.db import models
from django.contrib.auth.models import User

class Blog(models.Model):
    title = models.CharField(max_length=20)
    content = models.TextField()
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return self.title

class ProductType(models.Model):
    typename = models.CharField(max_length=50)

    def __str__(self):
        return self.typename

class Products(models.Model):
    name = models.CharField(max_length=20)
    description = models.TextField(max_length=500)
    image = models.ImageField(default="")
    product_type = models.ForeignKey(ProductType, on_delete=models.CASCADE)

    class Meta:
        ordering = ['product_type']

    def __str__(self):
        return self.name

class Block1(models.Model):
    title1 = models.CharField(max_length=50)
    content1 = models.TextField()
    title2 = models.CharField(max_length=50, blank=True)
    content2 = models.TextField(blank=True)
    image = models.ImageField(default="", blank=True)

    def __str__(self):
        return self.title1



class Block2(models.Model):
    title = models.CharField(max_length=50)
    content = models.TextField()
    def __str__(self):
        return self.title



class Block3(models.Model):
    title = models.CharField(max_length=50)
    content = models.TextField()
    def __str__(self):
        return self.title


class Block4(models.Model):
    title = models.CharField(max_length=50)
    image = models.ImageField(default="", blank=True)
    def __str__(self):
        return self.title


class Address(models.Model):
    en_company = models.CharField(max_length=100)
    cn_company = models.CharField(max_length=100)
    location = models.TextField()
    phone = models.CharField(max_length=30)
    domain = models.CharField(max_length=50)
